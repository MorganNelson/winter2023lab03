import java.util.Scanner;
public class ApplianceStore{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		ClothingIron[] appliances = new ClothingIron[4];
		for(int i=0;i<appliances.length;i++){
			System.out.println("appliance " +i);
			appliances[i] = new ClothingIron();
			System.out.println("Enter the maximum temperature that your appliance can reach");
			appliances[i].temperature = input.nextInt();
			System.out.println("Enter the weight of your appliance");
			appliances[i].weight = input.nextInt();
			do{
			System.out.println("Enter the current level of heat of your appliance, it go to a maximum of 3 and a minimum of 0");
			appliances[i].levelOfHeat = input.nextInt();}while(appliances[i].levelOfHeat >3 || appliances[i].levelOfHeat<0);
		}
		System.out.println("Temperature " + appliances[3].temperature +" , " + "Weight " + appliances[3].weight+ " , " + "Level of heat " + " , " + appliances[3].levelOfHeat);
		System.out.println("The temperature of your clothing iron is " + appliances[0].changeTemp(appliances[0].levelOfHeat, appliances[0].temperature));
		appliances[0].ironClothes(appliances[0].weight,appliances[0].temperature);
	}
}